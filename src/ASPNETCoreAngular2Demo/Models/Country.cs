﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace ASPNETCoreAngular2Demo.Models
{
    public class Country
    {
        /*
         * For disabling auto increment of attribute Key, add this: 
         * [DatabaseGeneratedAttribute(DatabaseGeneratedOption.Identity)]
         */
        public int Id { get; set; }
        public string Name { get; set; }

        /*public Country(string CountryName)
        {
            //Id = CountryID;
            Name = CountryName;
        }*/

    }
}