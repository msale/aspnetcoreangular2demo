﻿using System;
using System.Linq;
using Microsoft.Data.Entity;
using Microsoft.Extensions.DependencyInjection;

namespace ASPNETCoreAngular2Demo.Models
{
    public static class SampleData
    {
        public static void Initialize(IServiceProvider serviceProvider) {
            var context = serviceProvider.GetService<WorldContext>();
            context.Database.Migrate();
            if (!context.Countries.Any()) {
            
                Country eIsrael = context.Countries.Add(new Country { Name = "Israel2" }).Entity;
                Country eUsa = context.Countries.Add(new Country { Name = "Usa" }).Entity;
                Country eIndia = context.Countries.Add(new Country { Name = "India" }).Entity;
                Country eAustralia = context.Countries.Add(new Country { Name = "Australia" }).Entity;
                Country eCanada = context.Countries.Add(new Country { Name = "Canada" }).Entity;

                context.States.AddRange(
                    new State { Country = eIsrael, Name = "Tel-Aviv2" },
                    new State { Country = eIsrael, Name = "Haifa" },
                    new State { Country = eIsrael, Name = "Or-Yeuda" },
                    new State { Country = eIsrael, Name = "Hadera" },

                    new State { Country = eUsa, Name = "Arizona" },
                    new State { Country = eUsa, Name = "Alaska" },
                    new State { Country = eUsa, Name = "Florida" },
                    new State { Country = eUsa, Name = "Hawaii" },

                    new State { Country = eIndia, Name = "Gujarat" },
                    new State { Country = eIndia, Name = "Goa" },
                    new State { Country = eIndia, Name = "Punjab" },

                    new State { Country = eAustralia, Name = "Queensland" },
                    new State { Country = eAustralia, Name = "South Australia" },
                    new State { Country = eAustralia, Name = "Tasmania" },

                    new State { Country = eCanada, Name = "Alberta" },
                    new State { Country = eCanada, Name = "Ontario" },
                    new State { Country = eCanada, Name = "Quebec" },
                    new State { Country = eCanada, Name = "Saskatchewan" }
                );

                context.SaveChanges();

            }


        }
    }    
}
