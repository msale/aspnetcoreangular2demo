﻿using Microsoft.Data.Entity;

namespace ASPNETCoreAngular2Demo.Models
{
    public class WorldContext : DbContext
    {
        public DbSet<Country> Countries { get; set; }
        public DbSet<State> States { get; set; }

        /*
        //pluralize the database table names. source: https://gist.github.com/rowanmiller
        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            foreach (var entity in modelBuilder.Model.GetEntityTypes())
            {
                modelBuilder.Entity(entity.Name).ToTable(entity.Name + "s");
            }
        }*/
        
    }
}
 
 