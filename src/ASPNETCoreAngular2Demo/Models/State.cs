﻿using System;
using System.ComponentModel.DataAnnotations;

namespace ASPNETCoreAngular2Demo.Models
{
    public class State
    {

        [Key]
        public int Id { get; set; }
        public int CountryId { get; set; }
        public string Name { get; set; }

        //Navigation Property
        public Country Country { get; set; }

        /*public State( Country country, string stateName)
        {
            Country = country;
            Name = stateName;
        }*/

    }
}