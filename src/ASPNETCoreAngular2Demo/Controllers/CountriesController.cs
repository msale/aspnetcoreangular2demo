using System.Collections.Generic;
using System.Linq;
using Microsoft.AspNet.Http;
using Microsoft.AspNet.Mvc;
using Microsoft.Data.Entity;
using ASPNETCoreAngular2Demo.Models;

namespace ASPNETCoreAngular2Demo.Controllers
{
    [Produces("application/json")]
    [Route("api/Countries")]
    public class CountriesController : Controller
    {
        private WorldContext _context;

        public CountriesController(WorldContext context)
        {
            _context = context;
        }

        // GET: api/Countries/{id}
        [HttpGet]
        public IEnumerable<Country> GetCountry(int? id)
        {
            IEnumerable<Country>  countries = _context.Countries;

            if (id != null)
            {
                countries = countries.Where(c => c.Id == id);
            }

            return countries;

        }

        // PUT: api/Countries/5
        [HttpPut("{id}")]
        public IActionResult PutCountry(int id, [FromBody] Country country)
        {
            if (!ModelState.IsValid)
            {
                return HttpBadRequest(ModelState);
            }

            if (id != country.Id)
            {
                return HttpBadRequest();
            }

            _context.Entry(country).State = EntityState.Modified;

            try
            {
                _context.SaveChanges();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!CountryExists(id))
                {
                    return HttpNotFound();
                }
                else
                {
                    throw;
                }
            }

            return new HttpStatusCodeResult(StatusCodes.Status204NoContent);
        }

        // POST: api/Countries
        [HttpPost]
        public IActionResult PostCountry([FromBody] Country country)
        {
            if (!ModelState.IsValid)
            {
                return HttpBadRequest(ModelState);
            }

            _context.Countries.Add(country);
            try
            {
                _context.SaveChanges();
            }
            catch (DbUpdateException)
            {
                if (CountryExists(country.Id))
                {
                    return new HttpStatusCodeResult(StatusCodes.Status409Conflict);
                }
                else
                {
                    throw;
                }
            }

            return CreatedAtRoute("GetCountry", new { id = country.Id }, country);
        }

        // DELETE: api/Countries/5
        [HttpDelete("{id}")]
        public IActionResult DeleteCountry(int id)
        {
            if (!ModelState.IsValid)
            {
                return HttpBadRequest(ModelState);
            }

            Country country = null;
            try
            {
                country = _context.Countries.Single(m => m.Id == id);

                if (country == null)
                {
                    return HttpNotFound();
                }

                _context.Countries.Remove(country);
                _context.SaveChanges();

            }
            catch
            {
                return HttpNotFound();
            }

            return Ok(country);
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                _context.Dispose();
            }
            base.Dispose(disposing);
        }

        private bool CountryExists(int id)
        {
            return _context.Countries.Count(e => e.Id == id) > 0;
        }
    }
}