using System.Collections.Generic;
using System.Linq;
using Microsoft.AspNet.Http;
using Microsoft.AspNet.Mvc;
using Microsoft.Data.Entity;
using ASPNETCoreAngular2Demo.Models;

namespace ASPNETCoreAngular2Demo.Controllers
{
    [Produces("application/json")]
    [Route("api/States")]
    public class StatesController : Controller
    {
        private WorldContext _context;

        public StatesController(WorldContext context)
        {
            _context = context;
        }

        // GET: api/States/{id}{countryId}
        [HttpGet]
        public IEnumerable<State> GetState(int? id, int? countryId)
        {
            IEnumerable<State> states = _context.States;

            if (id != null)
            {
                states = states.Where(s => s.Id == id);
            }

            if (countryId != null)
            {
                states = states.Where(s => s.CountryId == countryId);
            }

            return states;

        }

        // PUT: api/States/5
        [HttpPut("{id}")]
        public IActionResult PutState(int id, [FromBody] State state)
        {
            if (!ModelState.IsValid)
            {
                return HttpBadRequest(ModelState);
            }

            if (id != state.Id)
            {
                return HttpBadRequest();
            }

            _context.Entry(state).State = EntityState.Modified;

            try
            {
                _context.SaveChanges();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!StateExists(id))
                {
                    return HttpNotFound();
                }
                else
                {
                    throw;
                }
            }

            return new HttpStatusCodeResult(StatusCodes.Status204NoContent);
        }

        // POST: api/States
        [HttpPost]
        public IActionResult PostState([FromBody] State state)
        {
            if (!ModelState.IsValid)
            {
                return HttpBadRequest(ModelState);
            }

            _context.States.Add(state);
            try
            {
                _context.SaveChanges();
            }
            catch (DbUpdateException)
            {
                if (StateExists(state.Id))
                {
                    return new HttpStatusCodeResult(StatusCodes.Status409Conflict);
                }
                else
                {
                    throw;
                }
            }

            return CreatedAtRoute("GetState", new { id = state.Id }, state);
        }

        // DELETE: api/States/5
        [HttpDelete("{id}")]
        public IActionResult DeleteState(int id)
        {
            if (!ModelState.IsValid)
            {
                return HttpBadRequest(ModelState);
            }
            State state = null;
            try
            {
                state = _context.States.Single(m => m.Id == id);
                if (state == null)
                {
                    return HttpNotFound();
                }

                _context.States.Remove(state);
                _context.SaveChanges();
            }
            catch
            {
                return HttpNotFound();
            }

            return Ok(state);
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                _context.Dispose();
            }
            base.Dispose(disposing);
        }

        private bool StateExists(int id)
        {
            return _context.States.Count(e => e.Id == id) > 0;
        }
    }
}