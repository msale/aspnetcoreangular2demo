using System;
using Microsoft.Data.Entity;
using Microsoft.Data.Entity.Infrastructure;
using Microsoft.Data.Entity.Metadata;
using Microsoft.Data.Entity.Migrations;
using ASPNETCoreAngular2Demo.Models;

namespace ASPNETCoreAngular2Demo.Migrations
{
    [DbContext(typeof(WorldContext))]
    [Migration("20160811182025_Init")]
    partial class Init
    {
        protected override void BuildTargetModel(ModelBuilder modelBuilder)
        {
            modelBuilder
                .HasAnnotation("ProductVersion", "7.0.0-rc1-16348")
                .HasAnnotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn);

            modelBuilder.Entity("ASPNETCoreAngular2Demo.Models.Country", b =>
                {
                    b.Property<int>("Id")
                        .ValueGeneratedOnAdd();

                    b.Property<string>("Name");

                    b.HasKey("Id");
                });

            modelBuilder.Entity("ASPNETCoreAngular2Demo.Models.State", b =>
                {
                    b.Property<int>("Id")
                        .ValueGeneratedOnAdd();

                    b.Property<int>("CountryId");

                    b.Property<string>("Name");

                    b.HasKey("Id");
                });

            modelBuilder.Entity("ASPNETCoreAngular2Demo.Models.State", b =>
                {
                    b.HasOne("ASPNETCoreAngular2Demo.Models.Country")
                        .WithMany()
                        .HasForeignKey("CountryId");
                });
        }
    }
}
