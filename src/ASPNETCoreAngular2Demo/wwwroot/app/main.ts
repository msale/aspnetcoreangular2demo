﻿import { bootstrap } from 'angular2/platform/browser';
import {HTTP_PROVIDERS} from "angular2/http";
import { CountryListComponent } from './components/countrylist.component';

bootstrap(CountryListComponent, [HTTP_PROVIDERS]);