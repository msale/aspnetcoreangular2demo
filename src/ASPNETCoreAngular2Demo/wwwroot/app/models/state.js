System.register([], function(exports_1, context_1) {
    "use strict";
    var __moduleName = context_1 && context_1.id;
    var State;
    return {
        setters:[],
        execute: function() {
            State = (function () {
                function State(id, countryid, name) {
                    this.id = id;
                    this.countryid = countryid;
                    this.name = name;
                }
                return State;
            }());
            exports_1("State", State);
        }
    }
});
//# sourceMappingURL=state.js.map