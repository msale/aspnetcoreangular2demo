﻿import { Component } from 'angular2/core';
import { DataService } from '../services/data.service';
import { DeviceService } from '../services/device.Service';
import { Country } from '../models/country';
import { OrderBy } from '../pipes/OrderBy.pipe';

@Component({
    selector: '[frame-countries-list]',
    templateUrl: 'app/views/FrameCountriesListTemplate.html',
    pipes: [OrderBy],
    providers: [DataService, DeviceService]
})
export class FrameCountriesList {
    countries: Country[];

    //TODO: is Styles needed?
    Styles: {
        aNumber: number; txtAlignOfCol: Function; deviceService: DeviceService
    } = {
        aNumber: undefined,
        txtAlignOfCol: function (colIndex: number) {
            var bootstrapDeviceType = this.deviceService.getBootstrapDeviceType().toLowerCase();
            if (bootstrapDeviceType == "sm" || bootstrapDeviceType == "xs") {
                return "center";
            } else if (colIndex == 1) {
                return "right";
            } else {
                return "left";
            }
        },
        deviceService: undefined
    };


    constructor(private _dataService: DataService, private _deviceService: DeviceService) {
        this._dataService.getCountries().subscribe(data => { this.countries = data });
        this.Styles.deviceService = _deviceService;
    }

    //TODO: is onSelect needed?
    onDelete(country) {
        this._dataService.delCountries(country.Id);

        let index = this.countries.indexOf(country);
        this.countries.splice(index, 1);

        return true;
    }

    onSelect(countryid) {
        return false;
    }

    isLen() {
        //return this.countries.length > 0;
        return this.countries;
    }

}
