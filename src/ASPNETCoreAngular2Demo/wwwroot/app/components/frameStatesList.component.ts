﻿import { Component } from 'angular2/core';
import { DataService } from '../services/data.service';
import { DeviceService } from '../services/device.Service';
import { Country } from '../models/country';
import { State } from '../models/state';
import { OrderBy } from '../pipes/OrderBy.pipe';

@Component({
    selector: '[frame-states-list]',
    templateUrl: 'app/views/FrameStatesListTemplate.html',
    pipes: [OrderBy],
    providers: [DataService, DeviceService]
})
export class FrameStatesList {
    states: State[] = [];
    
    //TODO: is Styles needed?
    Styles: {
        aNumber: number; txtAlignOfCol: Function; deviceService: DeviceService
    } = {
        aNumber: undefined,
        txtAlignOfCol: function (colIndex: number) {
            var bootstrapDeviceType = this.deviceService.getBootstrapDeviceType().toLowerCase();
            if (bootstrapDeviceType == "sm" || bootstrapDeviceType == "xs") {
                return "center";
            } else if (colIndex == 1) {
                return "right";
            } else {
                return "left";
            }
        },
        deviceService: undefined
    };

    constructor(private _dataService: DataService, private _deviceService: DeviceService) {
        this._dataService.getStates().subscribe(data => {
            //this.states = data;
            for (let i = 0; i < data.length; i++){
                var s = new State(data[i].Id, data[i].CountryId, data[i].Name);
                this.states[i] = s;
            }

        });
        this.Styles.deviceService = _deviceService;
    }

    //TODO: is onSelect needed?
    onDelete(state) {
        this._dataService.delStates(state.id);

        let index = this.states.indexOf(state);
        this.states.splice(index, 1);

        return true;
    }

    onSelect(stateid) {
        return false;
    }

    isLen() {
        //return this.countries.length > 0;
        return this.states;
    }

}
