System.register(['angular2/core', '../services/data.service', '../services/device.Service', '../pipes/OrderBy.pipe'], function(exports_1, context_1) {
    "use strict";
    var __moduleName = context_1 && context_1.id;
    var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
        var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
        if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
        else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
        return c > 3 && r && Object.defineProperty(target, key, r), r;
    };
    var __metadata = (this && this.__metadata) || function (k, v) {
        if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
    };
    var core_1, data_service_1, device_Service_1, OrderBy_pipe_1;
    var FrameCountriesList;
    return {
        setters:[
            function (core_1_1) {
                core_1 = core_1_1;
            },
            function (data_service_1_1) {
                data_service_1 = data_service_1_1;
            },
            function (device_Service_1_1) {
                device_Service_1 = device_Service_1_1;
            },
            function (OrderBy_pipe_1_1) {
                OrderBy_pipe_1 = OrderBy_pipe_1_1;
            }],
        execute: function() {
            FrameCountriesList = (function () {
                function FrameCountriesList(_dataService, _deviceService) {
                    var _this = this;
                    this._dataService = _dataService;
                    this._deviceService = _deviceService;
                    //TODO: is Styles needed?
                    this.Styles = {
                        aNumber: undefined,
                        txtAlignOfCol: function (colIndex) {
                            var bootstrapDeviceType = this.deviceService.getBootstrapDeviceType().toLowerCase();
                            if (bootstrapDeviceType == "sm" || bootstrapDeviceType == "xs") {
                                return "center";
                            }
                            else if (colIndex == 1) {
                                return "right";
                            }
                            else {
                                return "left";
                            }
                        },
                        deviceService: undefined
                    };
                    this._dataService.getCountries().subscribe(function (data) { _this.countries = data; });
                    this.Styles.deviceService = _deviceService;
                }
                //TODO: is onSelect needed?
                FrameCountriesList.prototype.onDelete = function (country) {
                    this._dataService.delCountries(country.Id);
                    var index = this.countries.indexOf(country);
                    this.countries.splice(index, 1);
                    return true;
                };
                FrameCountriesList.prototype.onSelect = function (countryid) {
                    return false;
                };
                FrameCountriesList.prototype.isLen = function () {
                    //return this.countries.length > 0;
                    return this.countries;
                };
                FrameCountriesList = __decorate([
                    core_1.Component({
                        selector: '[frame-countries-list]',
                        templateUrl: 'app/views/FrameCountriesListTemplate.html',
                        pipes: [OrderBy_pipe_1.OrderBy],
                        providers: [data_service_1.DataService, device_Service_1.DeviceService]
                    }), 
                    __metadata('design:paramtypes', [data_service_1.DataService, device_Service_1.DeviceService])
                ], FrameCountriesList);
                return FrameCountriesList;
            }());
            exports_1("FrameCountriesList", FrameCountriesList);
        }
    }
});
//# sourceMappingURL=frameCountriesList.component.js.map