﻿import { Component } from 'angular2/core';
import { DataService } from '../services/data.service';
import { DeviceService } from '../services/device.Service';
import { Country } from '../models/country';
import { State } from '../models/state';
import { FrameCountriesList } from './frameCountriesList.component';
import { FrameStatesList } from './frameStatesList.component';

@Component({
    selector: 'my-country-list',
    templateUrl: 'app/views/CountryTemplate.html',
    directives: [FrameCountriesList, FrameStatesList],
    providers: [DataService, DeviceService]
})
export class CountryListComponent {
    selectedCountry: Country = new Country(0, 'India');
    countries: Country[];
    states: State[];
    Styles: {
        aNumber: number; txtAlignOfCol: Function; deviceService: DeviceService
    } = {
        aNumber: undefined,
        txtAlignOfCol: function (colIndex: number) {
            var bootstrapDeviceType = this.deviceService.getBootstrapDeviceType().toLowerCase();
            if (bootstrapDeviceType == "sm" || bootstrapDeviceType == "xs") {
                return "center";
            } else if (colIndex == 1) {
                return "right";
            } else {
                return "left";
            }
        },
        deviceService: undefined
    };
    activeFrame: string;

    constructor(private _dataService: DataService, private _deviceService: DeviceService) {
        this._dataService.getCountries().subscribe(data => { this.countries = data });
        this.Styles.deviceService = _deviceService;
    }

    onSelect(countryid) {
        if (countryid == 0)
            this.states = null;
        else
            this._dataService.getStates(countryid).subscribe(data => { this.states = data });
    }

    getStyle() {
        return this.Styles;
    }

}
