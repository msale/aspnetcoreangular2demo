System.register(['angular2/core', '../services/data.service', '../services/device.Service', '../models/country', './frameCountriesList.component', './frameStatesList.component'], function(exports_1, context_1) {
    "use strict";
    var __moduleName = context_1 && context_1.id;
    var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
        var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
        if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
        else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
        return c > 3 && r && Object.defineProperty(target, key, r), r;
    };
    var __metadata = (this && this.__metadata) || function (k, v) {
        if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
    };
    var core_1, data_service_1, device_Service_1, country_1, frameCountriesList_component_1, frameStatesList_component_1;
    var CountryListComponent;
    return {
        setters:[
            function (core_1_1) {
                core_1 = core_1_1;
            },
            function (data_service_1_1) {
                data_service_1 = data_service_1_1;
            },
            function (device_Service_1_1) {
                device_Service_1 = device_Service_1_1;
            },
            function (country_1_1) {
                country_1 = country_1_1;
            },
            function (frameCountriesList_component_1_1) {
                frameCountriesList_component_1 = frameCountriesList_component_1_1;
            },
            function (frameStatesList_component_1_1) {
                frameStatesList_component_1 = frameStatesList_component_1_1;
            }],
        execute: function() {
            CountryListComponent = (function () {
                function CountryListComponent(_dataService, _deviceService) {
                    var _this = this;
                    this._dataService = _dataService;
                    this._deviceService = _deviceService;
                    this.selectedCountry = new country_1.Country(0, 'India');
                    this.Styles = {
                        aNumber: undefined,
                        txtAlignOfCol: function (colIndex) {
                            var bootstrapDeviceType = this.deviceService.getBootstrapDeviceType().toLowerCase();
                            if (bootstrapDeviceType == "sm" || bootstrapDeviceType == "xs") {
                                return "center";
                            }
                            else if (colIndex == 1) {
                                return "right";
                            }
                            else {
                                return "left";
                            }
                        },
                        deviceService: undefined
                    };
                    this._dataService.getCountries().subscribe(function (data) { _this.countries = data; });
                    this.Styles.deviceService = _deviceService;
                }
                CountryListComponent.prototype.onSelect = function (countryid) {
                    var _this = this;
                    if (countryid == 0)
                        this.states = null;
                    else
                        this._dataService.getStates(countryid).subscribe(function (data) { _this.states = data; });
                };
                CountryListComponent.prototype.getStyle = function () {
                    return this.Styles;
                };
                CountryListComponent = __decorate([
                    core_1.Component({
                        selector: 'my-country-list',
                        templateUrl: 'app/views/CountryTemplate.html',
                        directives: [frameCountriesList_component_1.FrameCountriesList, frameStatesList_component_1.FrameStatesList],
                        providers: [data_service_1.DataService, device_Service_1.DeviceService]
                    }), 
                    __metadata('design:paramtypes', [data_service_1.DataService, device_Service_1.DeviceService])
                ], CountryListComponent);
                return CountryListComponent;
            }());
            exports_1("CountryListComponent", CountryListComponent);
        }
    }
});
//# sourceMappingURL=countrylist.component.js.map