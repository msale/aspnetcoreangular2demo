﻿import { Injectable } from 'angular2/core';
import {Http, URLSearchParams} from "angular2/http";
import 'rxjs/Rx';
import { Country } from '../models/country';
import { State } from '../models/state';


@Injectable()
export class DataService {
    constructor(public http: Http) {
        this.http = http;
    }
    getCountries() {
        return this.http.get("/api/Countries")
            .map((responseData) => responseData.json());
    }

    getStates(countryid: string = "") {
        if (countryid != ""){
            var params = new URLSearchParams();
            params.set('countryId', countryid);
            return this.http.get("/api/States", { search: params })
                .map((responseData) => responseData.json());
        }

        return this.http.get("/api/States")
            .map((responseData) => responseData.json());

    }

    delCountries(countryid) {
        this.http.delete("/api/Countries/" + countryid).subscribe((res) => {
        });;
    }

    delStates(stateid) {
        this.http.delete("/api/States/" + stateid).subscribe((res) => {
        });;
    }
}
