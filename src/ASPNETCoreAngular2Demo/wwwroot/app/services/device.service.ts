﻿/// <reference path="../../../typings/jquery/jquery.d.ts" />

import { Injectable } from 'angular2/core';
import {Http, URLSearchParams} from "angular2/http";

declare var $: JQueryStatic;


@Injectable()
export class DeviceService {
    constructor(public http: Http) {
        this.http = http;
    }
    getBootstrapDeviceType() {
        var envs = ['xs', 'sm', 'md', 'lg'];

        var $el = $('<div>');
        $el.appendTo($('body'));

        for (var i = envs.length - 1; i >= 0; i--) {
            var env = envs[i];

            $el.addClass('hidden-' + env);
            if ($el.is(':hidden')) {
                $el.remove();
                return env;
            }
        }
    }

}
