System.register(['angular2/core', "angular2/http", 'rxjs/Rx'], function(exports_1, context_1) {
    "use strict";
    var __moduleName = context_1 && context_1.id;
    var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
        var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
        if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
        else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
        return c > 3 && r && Object.defineProperty(target, key, r), r;
    };
    var __metadata = (this && this.__metadata) || function (k, v) {
        if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
    };
    var core_1, http_1;
    var DataService;
    return {
        setters:[
            function (core_1_1) {
                core_1 = core_1_1;
            },
            function (http_1_1) {
                http_1 = http_1_1;
            },
            function (_1) {}],
        execute: function() {
            DataService = (function () {
                function DataService(http) {
                    this.http = http;
                    this.http = http;
                }
                DataService.prototype.getCountries = function () {
                    return this.http.get("/api/Countries")
                        .map(function (responseData) { return responseData.json(); });
                };
                DataService.prototype.getStates = function (countryid) {
                    if (countryid === void 0) { countryid = ""; }
                    if (countryid != "") {
                        var params = new http_1.URLSearchParams();
                        params.set('countryId', countryid);
                        return this.http.get("/api/States", { search: params })
                            .map(function (responseData) { return responseData.json(); });
                    }
                    return this.http.get("/api/States")
                        .map(function (responseData) { return responseData.json(); });
                };
                DataService.prototype.delCountries = function (countryid) {
                    this.http.delete("/api/Countries/" + countryid).subscribe(function (res) {
                    });
                    ;
                };
                DataService.prototype.delStates = function (stateid) {
                    this.http.delete("/api/States/" + stateid).subscribe(function (res) {
                    });
                    ;
                };
                DataService = __decorate([
                    core_1.Injectable(), 
                    __metadata('design:paramtypes', [http_1.Http])
                ], DataService);
                return DataService;
            }());
            exports_1("DataService", DataService);
        }
    }
});
//# sourceMappingURL=data.service.js.map